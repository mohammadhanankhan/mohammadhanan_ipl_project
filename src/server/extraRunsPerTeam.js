const { getDeliveriesData, getMatchesData } = require('./library');

/**
 * This function accepts the year ( IPL year )
 * and returns the extra runs conceded by the
 * teams in that year.
 *
 * @param {number} year IPL year for which we want extra runs for teams
 * @returns {object} contains the extra runs for teams for given year
 */

async function extraRunsPerTeam(year = 2016) {
  const IPL_START_YEAR = 2008;

  if (!Number.isInteger(year) || year < IPL_START_YEAR) {
    throw new Error();
  }

  const [deliveries,matches] = await Promise.all([getDeliveriesData(), getMatchesData()])

  // filtering given season matches to obtain matchIds and teamsInSeason
  const season = matches.filter(match => {
    const { season } = match;

    return +season === year;
  });

  // // Teams that played in that IPL season
  const teamsInSeason = season.reduce((accumulator, match) => {
    const { team1, team2 } = match;

    if (!(team1 in accumulator)) {
      accumulator[team1] = 0;
    } else if (!(team2 in accumulator)) {
      accumulator[team2] = 0;
    }

    return accumulator;
  }, {});

  // Match Ids
  const matchIds = season.map(match => match.id);

  //Filtering deliveries by match IDs
  const deliveriesInSeason = deliveries.filter(delivery => {
    const { match_id } = delivery;

    return matchIds.indexOf(match_id) !== -1;
  });

  // // Adding extra runs to each bowling team
  deliveriesInSeason.reduce((accumulator, delivery) => {
    const { bowling_team, extra_runs } = delivery;

    if (+extra_runs > 0) {
      accumulator[bowling_team] += +extra_runs;
    }
    return accumulator;
  }, teamsInSeason);

  return teamsInSeason;
}

module.exports = extraRunsPerTeam;
