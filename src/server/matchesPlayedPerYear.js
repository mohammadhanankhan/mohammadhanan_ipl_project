const { getMatchesData } = require('./library');

/**
 * This function has no parameters, it returns the number of matches
 * played in each edition of an IPL
 *
 * @returns {object} - contains number of matches played per season of an IPL
 */

async function matchesPlayedPerYear() {
  const matches = await getMatchesData();

  return matches.reduce((accumulator, match) => {
    const yearMatchPlayed = +match.season;

    if (yearMatchPlayed in accumulator) {
      accumulator[yearMatchPlayed] += 1;
    } else {
      accumulator[yearMatchPlayed] = 1;
    }

    return accumulator;
  }, {});
}

module.exports = matchesPlayedPerYear;
