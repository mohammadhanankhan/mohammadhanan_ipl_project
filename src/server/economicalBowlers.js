const { getDeliveriesData, getMatchesData } = require('./library');

/**
 * This function will accept the year ( IPL year )
 * and number as parameter and return those number of
 * top economical bowlers for that year in an object
 *
 * @param {number} year  - IPL year for which we want economical bowlers
 * @param {number} number - Number of economical bowlers we want
 * @returns {object} - Contains top economical bowlers
 */

async function economicalBowlers(year = 2015, number = 10) {
  const IPL_START_YEAR = 2008;

  if (
    !Number.isInteger(year) &&
    year < IPL_START_YEAR &&
    !Number.isInteger(number)
  ) {
    throw new Error();
  }

  const [deliveries,matches] = await Promise.all([getDeliveriesData(), getMatchesData()])

  // filtering given season matches to obtain matchIds
  const season = matches.filter(match => {
    const { season } = match;

    return +season === year;
  });

  // Match Ids
  const matchIds = season.map(match => match.id);

  //Filtering deliveries by match IDs
  const deliveriesInSeason = deliveries.filter(delivery => {
    const { match_id } = delivery;

    return matchIds.indexOf(match_id) !== -1;
  });

  // Bowlers who bowled in that season
  const bowlersInSeason = deliveriesInSeason.reduce((accumulator, delivery) => {
    const { bowler } = delivery;

    if (!(bowler in accumulator)) {
      accumulator[bowler] = {
        ballsBowled: 0,
        runsConceded: 0,
      };
    }

    return accumulator;
  }, {});

  // adding 'ballsBowled' and 'runsConceded'
  deliveriesInSeason.reduce((accumulator, delivery) => {
    const { bowler, wide_runs, noball_runs, batsman_runs } = delivery;

    if (+wide_runs === 0 && +noball_runs === 0) {
      accumulator[bowler]['ballsBowled'] += 1;
    }

    accumulator[bowler]['runsConceded'] +=
      +wide_runs + +noball_runs + +batsman_runs;

    return accumulator;
  }, bowlersInSeason);

  // Adding the 'oversBowled' and 'economy_rate'
  for (let bowler in bowlersInSeason) {
    const { ballsBowled } = bowlersInSeason[bowler];

    const overs = Math.floor(ballsBowled / 6);
    const remaining = ballsBowled % 6;

    // Overs bowled by a bowler
    bowlersInSeason[bowler]['oversBowled'] = +`${overs}.${remaining}`;

    const { oversBowled, runsConceded } = bowlersInSeason[bowler];

    // setting economy directly by reassigning bowler object
    bowlersInSeason[bowler] = (runsConceded / oversBowled).toFixed(2);
  }

  // top economicalBowlers as nested array
  const topEconomicalBowlers = Object.entries(bowlersInSeason)
    .sort((a, b) => a[1] - b[1])
    .slice(0, number);

  return Object.fromEntries(topEconomicalBowlers);
}

module.exports = economicalBowlers;
