const { getDeliveriesData } = require('./library');

/**
 * This asynchronous function has no parameters, it works on the
 * data which is an array of all deliveries obtained by calling
 * the function 'getDeliveriesArray()'
 *
 * @returns {Object} - The most economical bowler in Super-overs.
 */

async function bestEconomyInSuperOvers() {
  const deliveries = await getDeliveriesData();

  // console.log(deliveries.filter(del => del.is_super_over !== '0'));

  const bowlers = deliveries.reduce((accumulator, delivery) => {
    const { is_super_over, bowler, batsman_runs, wide_runs, noball_runs } =
      delivery;

    if (is_super_over !== '0') {
      if (!(bowler in accumulator)) {
        accumulator[bowler] = {
          ballsBowled: 0,
          runsConceded: 0,
        };
      } else {
        if (+wide_runs === 0 && +noball_runs === 0) {
          accumulator[bowler].ballsBowled += 1;
        }

        accumulator[bowler].runsConceded +=
          +wide_runs + +noball_runs + +batsman_runs;
      }
    }

    return accumulator;
  }, {});

  for (let bowler in bowlers) {
    const { ballsBowled } = bowlers[bowler];

    const overs = Math.floor(ballsBowled / 6);
    const remaining = ballsBowled % 6;

    // Overs bowled by a bowler
    bowlers[bowler]['oversBowled'] = +`${overs}.${remaining}`;

    // Bowlers economy upto 2 decimal places
    const { oversBowled, runsConceded } = bowlers[bowler];

    bowlers[bowler] = (runsConceded / oversBowled).toFixed(2);
  }

  const economicalBowler = Object.fromEntries(
    Object.entries(bowlers)
      .sort((a, b) => a[1] - b[1])
      .slice(0, 1)
  );

  return economicalBowler;
}

module.exports = bestEconomyInSuperOvers;
