const { getMatchesData } = require('./library');

/**
 * This function has no parameters and returns the total number of
 * times each team won the toss and also won the match
 *
 * @returns {object} - contains the number of times each team won the match while
 * winning the toss as well
 */

async function matchesWonWithToss() {
  const matches = await getMatchesData();

  const teams = matches.reduce((accumulator, match) => {
    const { team1, team2 } = match;

    if (!(team1 in accumulator)) {
      accumulator[team1] = 0;
    } else if (!(team2 in accumulator)) {
      accumulator[team2] = 0;
    }

    return accumulator;
  }, {});

  matches.forEach(match => {
    const { toss_winner, winner } = match;

    if (toss_winner === winner) {
      teams[winner] += 1;
    }
  });

  return teams;
}

module.exports = matchesWonWithToss;
