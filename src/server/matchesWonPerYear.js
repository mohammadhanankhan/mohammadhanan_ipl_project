const { getMatchesData } = require('./library');

/**
 * This function has no parameters, and returns the number of
 * matches each team in an IPL has won in each season.
 *
 * @returns {object} - contains number of matches each team has won in each season
 */

async function matchesWonPerYear() {
  const matches = await getMatchesData();

  // Obtaining all the teams from all the seasons
  const teams = matches.reduce((accumulator, match) => {
    const { team1, team2 } = match;

    if (!(team1 in accumulator)) {
      accumulator[team1] = {};
    } else if (!(team2 in accumulator)) {
      accumulator[team2] = {};
    }

    return accumulator;
  }, {});

  matches.reduce((accumulator, match) => {
    const { winner, season } = match;

    if (winner in teams && season in teams[winner]) {
      accumulator[winner][season] += 1;
    } else if (winner in teams && !(season in teams[winner])) {
      accumulator[winner][season] = 1;
    }

    return accumulator;
  }, teams);

  return teams;
}

module.exports = matchesWonPerYear;
