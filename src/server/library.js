const path = require('path');
const csvtojson = require('csvtojson');

const matchesPath = path.join(__dirname, '../', '/data/matches.csv');
const deliveriesPath = path.join(__dirname, '../', '/data/deliveries.csv');

function getMatchesData() {
  const matchesData = csvtojson().fromFile(matchesPath);

  return matchesData;
}
function getDeliveriesData() {
  const deliveriesData = csvtojson().fromFile(deliveriesPath);

  return deliveriesData;
}

module.exports = {
  getMatchesData,
  getDeliveriesData,
};
