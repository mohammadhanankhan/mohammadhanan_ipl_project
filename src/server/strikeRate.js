const { getMatchesData, getDeliveriesData } = require('./library');

/**
 * This function accepts a player as a parameter and returns
 * the strike rate for that player for each season
 * @param {string} player - Player for which strike rate for the season is required
 * @returns {object} - contains the strike rates of a player in that season
 */

async function strikeRate(player = 'S Dhawan') {
  const IPL_START_YEAR = 2008;

  if (typeof player !== 'string') {
    throw new Error();
  }
  const [deliveries,matches] = await Promise.all([getDeliveriesData(), getMatchesData()])


  const years = matches
    .map(match => match.season)
    .filter((match, index, matches) => matches.indexOf(match) === index);

  const batsmanStrikeRate = years.reduce((accumulator, year) => {
    const strikeRate = getStrikeRate(deliveries, matches, year, player);

    accumulator[year] = strikeRate;

    return accumulator;
  }, {});

  return batsmanStrikeRate;
}

function getStrikeRate(deliveries, matches, year, player) {
  // obtaining the season for which strike rate is required

  const season = matches.filter(match => {
    const { season } = match;

    return +season === +year;
  });

  // Match Ids
  const matchIds = season.map(match => match.id);

  //Filtering deliveries by match IDs
  const deliveriesInSeason = deliveries.filter(delivery => {
    const { match_id } = delivery;

    return matchIds.indexOf(match_id) !== -1;
  });

  // Runs and balls faced by the player in the particular year
  let onStrike = deliveriesInSeason.reduce(
    (accumulator, delivery) => {
      const { batsman, batsman_runs, wide_runs } = delivery;

      if (batsman === player && wide_runs === '0') {
        accumulator.ballsFaced += 1;
        accumulator.runsScored += +batsman_runs;
      }

      return accumulator;
    },
    {
      runsScored: 0,
      ballsFaced: 0,
    }
  );

  return ((onStrike.runsScored * 100) / onStrike.ballsFaced).toFixed(2);
}

module.exports = strikeRate;
