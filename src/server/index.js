const fs = require('fs');
const path = require('path');
const matchesPlayedPerYear = require('./matchesPlayedPerYear.js');
const matchesWonPerYear = require('./matchesWonPerYear');
const extraRunsPerTeam = require('./extraRunsPerTeam');
const economicalBowlers = require('./economicalBowlers');
const matchesWonWithToss = require('./matchesWonWithToss');
const highestNumberOfAwards = require('./highestNumberOfAwards.js');
const strikeRate = require('./strikeRate');
const highestDismissals = require('./highestDismissals');
const bestEconomyInSuperOvers = require('./bestEconomyInSuperOvers');

// To check if the output directory exists or not - add if true
if (!fs.existsSync(path.join(__dirname, '../', 'public/output'))) {
  fs.mkdirSync(path.join(__dirname, '../', 'public/output'));
}

matchesPlayedPerYear()
  .then(matchesPlayedPerYearResult => {
    fs.writeFile(
      path.join(__dirname, '../', 'public/output', 'matchesPerYear.json'),

      JSON.stringify(matchesPlayedPerYearResult, null, 2),

      err => {
        if (err) {
          console.log(err);
        }
      }
    );
  })
  .catch(err => {
    console.log(err);
  });

matchesWonPerYear()
  .then(matchesWonPerYearResult => {
    fs.writeFile(
      path.join(__dirname, '../', 'public/output', 'matchesWonPerYear.json'),

      JSON.stringify(matchesWonPerYearResult, null, 2),

      err => {
        if (err) {
          console.log(err);
        }
      }
    );
  })
  .catch(err => {
    console.log(err);
  });

extraRunsPerTeam()
  .then(extraRunsPerTeamResult => {
    fs.writeFile(
      path.join(__dirname, '../', 'public/output', 'extraRunsPerTeam.json'),

      JSON.stringify(extraRunsPerTeamResult, null, 2),

      err => {
        if (err) {
          console.log(err);
        }
      }
    );
  })
  .catch(err => {
    console.log(err);
  });

economicalBowlers()
  .then(economicalBowlersResult => {
    fs.writeFile(
      path.join(__dirname, '../', 'public/output', 'economicalBowlers.json'),

      JSON.stringify(economicalBowlersResult, null, 2),

      err => {
        if (err) {
          console.log(err);
        }
      }
    );
  })
  .catch(err => {
    console.log(err);
  });

matchesWonWithToss()
  .then(matchesWonWithTossResult => {
    fs.writeFile(
      path.join(__dirname, '../', 'public/output', 'matchesWonWithToss.json'),

      JSON.stringify(matchesWonWithTossResult, null, 2),

      err => {
        if (err) {
          console.log(err);
        }
      }
    );
  })
  .catch(err => {
    console.log(err);
  });

highestNumberOfAwards()
  .then(highestNumberOfAwardsResult => {
    fs.writeFile(
      path.join(
        __dirname,
        '../',
        'public/output',
        'highestNumberOfAwards.json'
      ),

      JSON.stringify(highestNumberOfAwardsResult, null, 2),

      err => {
        if (err) {
          console.log(err);
        }
      }
    );
  })
  .catch(err => {
    console.log(err);
  });

strikeRate()
  .then(strikeRateResult => {
    fs.writeFile(
      path.join(__dirname, '../', 'public/output', 'strikeRate.json'),

      JSON.stringify(strikeRateResult, null, 2),

      err => {
        if (err) {
          console.log(err);
        }
      }
    );
  })
  .catch(err => {
    console.log(err);
  });

highestDismissals()
  .then(highestDismissalsResult => {
    fs.writeFile(
      path.join(__dirname, '../', 'public/output', 'highestDismissals.json'),

      JSON.stringify(highestDismissalsResult, null, 2),

      err => {
        if (err) {
          console.log(err);
        }
      }
    );
  })
  .catch(err => {
    console.log(err);
  });

bestEconomyInSuperOvers()
  .then(bestEconomyInSuperOversResult => {
    fs.writeFile(
      path.join(
        __dirname,
        '../',
        'public/output',
        'bestEconomyInSuperOvers.json'
      ),

      JSON.stringify(bestEconomyInSuperOversResult, null, 2),

      err => {
        if (err) {
          console.log(err);
        }
      }
    );
  })
  .catch(err => {
    console.log(err);
  });
