const { getDeliveriesData } = require('./library');

/**
 * This function accepts a player name and returns the highest
 * number of dismissals of the player and the bowler with those
 * dismissals
 *
 * @param {string} player - player for which highest dismissals against a bowler are   required
 * @returns {object} - contains bowler and the number of dismissals the player has against that player
 */

async function highestDismissals(player = 'S Dhawan') {
  if (typeof player !== 'string') {
    throw new Error();
  }
  const deliveries = await getDeliveriesData();

  const batsman = deliveries.reduce((accumulator, delivery) => {
    const { player_dismissed, bowler } = delivery;

    if (player_dismissed === player) {
      if (accumulator[bowler] === undefined) {
        accumulator[bowler] = 0;
      }
      accumulator[bowler] += 1;
    }
    return accumulator;
  }, {});

  const maxDismissals = Math.max(...Object.values(batsman));

  for (let dismissal in batsman) {
    if (batsman[dismissal] !== maxDismissals) {
      delete batsman[dismissal];
    }
  }
  return batsman;
}

module.exports = highestDismissals;
