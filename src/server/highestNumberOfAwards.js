const { getMatchesData } = require('./library');

/**
 * This function accepts year parameter and returns the
 * highest number of player of the series awards for that
 * season (year) in an IPL
 * @param {number} year  - IPL year for which we want highest player of the series awards
 * @returns {object} - contains the player with most player of hte series awards for each season
 */

async function highestNumberOfAwards(year = 2015) {
  const IPL_START_YEAR = 2008;

  if (!Number.isInteger(year) || year < IPL_START_YEAR) {
    throw new Error();
  }

  const matches = await getMatchesData();

  const seasons = matches.reduce((accumulator, match) => {
    const { season, player_of_match } = match;

    if (!(season in accumulator)) {
      accumulator[season] = {};
    }

    if (player_of_match in accumulator[season]) {
      accumulator[season][player_of_match] += 1;
    } else {
      accumulator[season][player_of_match] = 1;
    }

    return accumulator;
  }, {});

  for (let season in seasons) {
    let maximumAwards = Math.max(...Object.values(seasons[season]));

    for (let player in seasons[season]) {
      if (seasons[season][player] !== maximumAwards) {
        delete seasons[season][player];
      }
    }
  }

  return seasons[year];
}

module.exports = highestNumberOfAwards;
